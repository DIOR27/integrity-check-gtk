/*
	Copyright (C) 2015-2020. Vinari Software.
	All rights reserved.​


	Redistribution and use in source and binary forms, source code with or without
	modification, are permitted provided that the following conditions are met:

	1. Redistributions of source code must retain the above copyright
	notice, this list of conditions and the following disclaimer.

	2. Redistributions in binary form must reproduce the above copyright
	notice, this list of conditions and the following disclaimer in the
	documentation and/or other materials provided with the distribution.

	3. All advertising materials mentioning features or use of this software
	must display the following acknowledgement:
	This product includes software developed by Vinari Software.

	4. Neither the name of the Vinari Software nor the
	names of its contributors may be used to endorse or promote products
	derived from this software without specific prior written permission.
 

	THIS SOFTWARE IS PROVIDED BY VINARI SOFTWARE "AS IS" AND ANY
	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL VINARI SOFTWARE BE LIABLE FOR ANY
	DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 

	VINARI SOFTWARE 2015-2020
*/

using Gtk;

//https://docs.elementary.io/develop/apis/actions

namespace GUI{
	public class AboutWindow : Gtk.AboutDialog {
		const string LicenseLocation="/usr/share/doc/integrity-check-vinarisoftware/LICENSE";
		private string licenseTMP;

		construct{

			try{
				FileUtils.get_contents(LicenseLocation, out licenseTMP);
			}catch(FileError e){
				error("%s", e.message);
			}
			
			this.title=_("Integrity Check - Vinari Software | About");
			this.window_position=Gtk.WindowPosition.CENTER_ON_PARENT;
			this.set_modal(true);

			this.program_name="Integrity Check - Vinari Software";
			this.comments=_("Integrity Check - Vinari Software is a software application\n designed to calculate the verifications sum of selected files and compare them\n against the sum provided by the file creator.");
			this.version="1.0.0";
			this.copyright="Copyright 2015 - 2020 Vinari Software";
			this.website="https://vinarisoftware.wixsite.com/vinari/";
			this.website_label="Vinari Software";

			this.license=licenseTMP;
			this.wrap_license=true;

			this.translator_credits="Vinari Software - Español";

			try{
				Gdk.Pixbuf mainIcon=new Gdk.Pixbuf.from_file(Application.AssetLocation+"About-Icon.png");
				this.set_logo(mainIcon);
				this.set_icon(mainIcon);
			}catch(Error e){
				error("Unable to load file: %s", e.message);
			}

			this.response.connect ((response_id) => {														//If the close button shows up in any version of GTK this function enables it to close the Dialog
				if (response_id == Gtk.ResponseType.CANCEL || response_id == Gtk.ResponseType.DELETE_EVENT) {
					this.hide_on_delete ();
				}
			});
		}
	}
}
