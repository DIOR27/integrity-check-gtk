/*
	Copyright (C) 2015-2020. Vinari Software.
	All rights reserved.​


	Redistribution and use in source and binary forms, source code with or without
	modification, are permitted provided that the following conditions are met:

	1. Redistributions of source code must retain the above copyright
	notice, this list of conditions and the following disclaimer.

	2. Redistributions in binary form must reproduce the above copyright
	notice, this list of conditions and the following disclaimer in the
	documentation and/or other materials provided with the distribution.

	3. All advertising materials mentioning features or use of this software
	must display the following acknowledgement:
	This product includes software developed by Vinari Software.

	4. Neither the name of the Vinari Software nor the
	names of its contributors may be used to endorse or promote products
	derived from this software without specific prior written permission.
 

	THIS SOFTWARE IS PROVIDED BY VINARI SOFTWARE "AS IS" AND ANY
	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL VINARI SOFTWARE BE LIABLE FOR ANY
	DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 

	VINARI SOFTWARE 2015-2020
*/

using Gtk;

//https://docs.elementary.io/develop/apis/actions

namespace GUI{
	public class SettingsWindow : Gtk.ApplicationWindow {
		const int MD5=7894;
		const int SHA1=7093;
		const int SHA256=7092;

		private Gtk.Label informationLabel;
		private Gtk.RadioButton MD5Check;
		private Gtk.RadioButton SHA1Check;
		private Gtk.RadioButton SHA256Check;
		private Gtk.Button okButton;
		private int selectedSum;

		construct{
			this.set_default_size(355, 120);
			this.set_modal(true);
			this.set_title(_("Integrity Check - Vinari Software | Settings"));
			this.window_position=Gtk.WindowPosition.CENTER_ON_PARENT;
			this.set_border_width(10);
			this.set_resizable(false);

			try{
				Gdk.Pixbuf mainIcon=new Gdk.Pixbuf.from_file(Application.AssetLocation+"IntegrityCheck-Icon.png");
				this.set_icon(mainIcon);
			}catch(Error e){
				error("Unable to load file: %s", e.message);
			}

			Gtk.Box mainBox=new Gtk.Box(Gtk.Orientation.VERTICAL, 5);
			Gtk.Box optionsBox=new Gtk.Box(Gtk.Orientation.HORIZONTAL, 7);

			MD5Check=new Gtk.RadioButton(null);
			MD5Check.set_label("MD5");
			SHA1Check=new Gtk.RadioButton.with_label(MD5Check.get_group(), "SHA1");
			SHA256Check=new Gtk.RadioButton.with_label(MD5Check.get_group(), "SHA256");

			informationLabel=new Gtk.Label(_("Select the verification sum you want to calculate"));
			okButton=new Gtk.Button.with_label(_("Apply changes"));

			okButton.clicked.connect(okButtonAction);
			this.key_press_event.connect(key_shortcuts);

			optionsBox.pack_start(MD5Check, false, false, 5);
			optionsBox.pack_start(SHA1Check, false, false, 5);
			optionsBox.pack_start(SHA256Check, false, false, 5);

			mainBox.pack_start(informationLabel, true, false, 5);
			mainBox.pack_start(optionsBox, true, false, 5);
			optionsBox.set_halign(CENTER);
			mainBox.pack_start(okButton, false, false, 5);

			this.selectedSum=MainWindow.verificationSUM;

			switch(selectedSum){
				case MD5:{
					MD5Check.set_active(true);
				}
				break;

				case SHA1:{
					SHA1Check.set_active(true);
				}
				break;

				case SHA256:{
					SHA256Check.set_active(true);
				}
				break;

				default:{
					MD5Check.set_active(false);
					SHA256Check.set_active(false);
					SHA1Check.set_active(false);
				}
				break;
			}
			
			this.add(mainBox);
		}

		private bool key_shortcuts(Gdk.EventKey key){
			switch(key.keyval){
				case Gdk.Key.Escape:{
					this.destroy();
				}
				break;
			}
			return false;
		}
		
		private void okButtonAction(){
			if(MD5Check.get_active()==true){
				MainWindow.verificationSUM=MD5;
			}else if(SHA1Check.get_active()==true){
				MainWindow.verificationSUM=SHA1;
			}else if(SHA256Check.get_active()==true){
				MainWindow.verificationSUM=SHA256;
			}
			this.destroy();
		}
	}
}
