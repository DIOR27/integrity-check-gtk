/*
	Copyright (C) 2015-2020. Vinari Software.
	All rights reserved.​


	Redistribution and use in source and binary forms, source code with or without
	modification, are permitted provided that the following conditions are met:

	1. Redistributions of source code must retain the above copyright
	notice, this list of conditions and the following disclaimer.

	2. Redistributions in binary form must reproduce the above copyright
	notice, this list of conditions and the following disclaimer in the
	documentation and/or other materials provided with the distribution.

	3. All advertising materials mentioning features or use of this software
	must display the following acknowledgement:
	This product includes software developed by Vinari Software.

	4. Neither the name of the Vinari Software nor the
	names of its contributors may be used to endorse or promote products
	derived from this software without specific prior written permission.
 

	THIS SOFTWARE IS PROVIDED BY VINARI SOFTWARE "AS IS" AND ANY
	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL VINARI SOFTWARE BE LIABLE FOR ANY
	DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 

	VINARI SOFTWARE 2015-2020
*/

using Gtk;
using App.Widgets;

//https://docs.elementary.io/develop/apis/actions

namespace GUI{
	public class MainWindow : Gtk.ApplicationWindow {

		const int MD5=7894;
		const int SHA1=7093;
		const int SHA256=7092;
		
		private App.Widgets.AppHeaderBar headerBar;
		private App.Widgets.MessageDialog messageBox;
		
		private Gtk.Entry sumField1;
		private Gtk.Entry sumField2;
		private Gtk.CheckButton checkButton1;
		private Gtk.Button compareButton;
		private Gtk.Button resetButton;
		private Gtk.Image imageResult;
		private Gtk.Label labelExplanation1;
		private Gtk.Label labelExplanation2;
		private Gtk.PopoverMenu popMenu;
		private Gtk.ModelButton preferencesButton;
		private Gtk.ModelButton openFileButton;
		private Gtk.ModelButton aboutButton;
		private Gtk.ModelButton quitButton;
		private Gtk.FileChooserDialog openFileDialog;

		private File fileSelected;
		private string fileName;
		public static int verificationSUM;
		private GLib.Settings mySettings=new GLib.Settings("org.vinarisoftware.integritycheck");
		
		public MainWindow(Application aplicacionV){
			Object(application: aplicacionV);
		}

		construct{
			this.move(mySettings.get_int("pos-x"), mySettings.get_int("pos-y"));
			this.set_default_size(340, 400);
			this.set_title("Integrity Check - Vinari Software");
			this.set_border_width(10);
			this.set_resizable(false);

			verificationSUM=mySettings.get_int("verification-selected");

			try{
				Gdk.Pixbuf mainIcon=new Gdk.Pixbuf.from_file(Application.AssetLocation+"IntegrityCheck-Icon.png");
				this.set_icon(mainIcon);
			}catch(Error e){
				error("Unable to load file: %s", e.message);
			}

			Gtk.Box menuBox=new Gtk.Box(Gtk.Orientation.VERTICAL, 5);
			Gtk.Box verticalMainBox=new Gtk.Box(Gtk.Orientation.VERTICAL, 5);
			Gtk.Box controlBox=new Gtk.Box(Gtk.Orientation.VERTICAL, 2);
			Gtk.Box resultBox=new Gtk.Box(Gtk.Orientation.VERTICAL, 2);
			Gtk.Box box1=new Gtk.Box(Gtk.Orientation.VERTICAL, 2);
			Gtk.Box box2=new Gtk.Box(Gtk.Orientation.VERTICAL, 2);

			preferencesButton=new Gtk.ModelButton();
			aboutButton=new Gtk.ModelButton();
			openFileButton=new Gtk.ModelButton();
			quitButton=new Gtk.ModelButton();
			
			preferencesButton.set_label(_("Settings"));
			openFileButton.set_label(_("Get hash of a file"));			
			aboutButton.set_label(_("About"));
			quitButton.set_label(_("Quit"));

			menuBox.pack_start(openFileButton, false, false, 2);
			menuBox.pack_start(preferencesButton, false, false, 2);
			menuBox.pack_start(aboutButton, false, false, 2);
			menuBox.pack_start(quitButton, false, false, 2);
			
			popMenu=new Gtk.PopoverMenu();
			popMenu.add(menuBox);
			popMenu.set_position(Gtk.PositionType.BOTTOM);
			
			headerBar=new App.Widgets.AppHeaderBar();

			imageResult=new Gtk.Image.from_file(Application.AssetLocation+"PlaceHolder.png");
			
			labelExplanation1=new Gtk.Label(_("Type the verification sum you got from the file:"));
			labelExplanation2=new Gtk.Label(_("Type the verification sum of the file that should be:"));

			compareButton=new Gtk.Button.with_label(_("Compare sums."));
			resetButton=new Gtk.Button.with_label(_("New comparison."));
			
			checkButton1=new Gtk.CheckButton.with_label(_("Ignore uppercase and lowercase characters."));
			checkButton1.set_active(mySettings.get_boolean("ignore-capitalization"));

			sumField1=new Gtk.Entry();
			sumField2=new Gtk.Entry();

			resultBox.pack_start(imageResult, false, false, 2);
			
			box1.pack_start(labelExplanation1, false, false, 2);
			box1.pack_start(sumField1, false, false, 2);

			box2.pack_start(labelExplanation2, false, false, 2);
			box2.pack_start(sumField2, false, false, 2);

			controlBox.pack_start(compareButton, false, false, 2);
			controlBox.pack_start(resetButton, false, false, 2);
			controlBox.pack_start(checkButton1, false, true, 2);
			controlBox.set_halign(CENTER);
			
			verticalMainBox.pack_start(resultBox, false, false, 0);
			verticalMainBox.pack_start(box1, false, false, 15);
			verticalMainBox.pack_start(box2, false, false, 7);
			verticalMainBox.pack_start(controlBox, false, false, 5);

			compareButton.clicked.connect(compareButtonAction);
			resetButton.clicked.connect(resetButtonAction);
			headerBar.menuButton.clicked.connect(menuButtonAction);

			quitButton.clicked.connect(quitButtonAction);
			openFileButton.clicked.connect(openFileButtonAction);
			preferencesButton.clicked.connect(preferencesButtonAction);
			aboutButton.clicked.connect(aboutButtonAction);
			
			delete_event.connect(e =>{
				return before_destroy();
			});

			this.add(verticalMainBox);
			this.set_titlebar(headerBar);
			this.show_all();
		}

		private void openResponse(Gtk.Dialog dialog, int response_id){
			var open_dialog = dialog as Gtk.FileChooserDialog;
			
			switch (response_id) {
				case Gtk.ResponseType.ACCEPT:{
					fileName=open_dialog.get_uri();

					if(fileName.has_prefix("file://")){
						fileSelected = File.new_for_uri(fileName);
					}
					switch(verificationSUM){
						case MD5:{
							getMD5Sum(fileSelected);
						}
						break;

						case SHA1:{
							getSHA1Sum(fileSelected);
						}
						break;

						case SHA256:{
							getSHA256Sum(fileSelected);
						}
						break;
					}
				}
				break;

				case Gtk.ResponseType.CANCEL:{
					print ("File seletcion cancelled.\n");
				}
				break;
			}
			dialog.destroy();
		}
		
		private void openFileButtonAction(){
			Gtk.FileFilter fileFilter=new Gtk.FileFilter();
			fileFilter.set_name(_("All files"));
			fileFilter.add_pattern("*");
			
			openFileDialog = new Gtk.FileChooserDialog(_("Choose a file..."), this, Gtk.FileChooserAction.OPEN, _("Cancel"), Gtk.ResponseType.CANCEL, _("Open"), Gtk.ResponseType.ACCEPT);
			openFileDialog.add_filter(fileFilter);
			openFileDialog.set_default_size(725, 440);
			openFileDialog.set_local_only(false);
			openFileDialog.set_modal(true);
			openFileDialog.response.connect(openResponse);
			openFileDialog.show();
		}
		
		private void menuButtonAction(){
			popMenu.set_relative_to(headerBar.menuButton);
			popMenu.set_modal(true);
			popMenu.show_all();
			popMenu.popup();
		}
		
		private void compareButtonAction(){

			string sum1=sumField1.get_text();
			string sum2=sumField2.get_text();

			if(sum1=="" || sum2==""){
				messageBox=new App.Widgets.MessageDialog(this, ERROR, OK, _("One of the two text fields is empty.\nThe verification can not be done."), "Integrity Check - Vinari Software");
				return;
			}
			
			sumField1.set_sensitive(false);
			sumField2.set_sensitive(false);
			compareButton.set_sensitive(false);

			if(checkButton1.get_active()==true){
				sum1=sum1.down();
				sum2=sum2.down();
			}

			if(sum1==sum2){
				imageResult.set_from_file(Application.AssetLocation+"OK.png");
				messageBox=new App.Widgets.MessageDialog(this, INFO, OK, _("The verification sums match perfectly, therefore, your file is intact."), "Integrity Check - Vinari Software");
			}else{
				imageResult.set_from_file(Application.AssetLocation+"Warning.png");
				messageBox=new App.Widgets.MessageDialog(this, WARNING, OK, _("The verification sums don't match at all.\n Is possible that the file is altered, corrupted, or incomplete."), "Integrity Check - Vinari Software");
			}	
		}

		private void quitButtonAction(){
			before_destroy();
			this.destroy();
		}

		private void preferencesButtonAction(){
			SettingsWindow settingsWindowForm=new SettingsWindow();
			settingsWindowForm.set_transient_for(this);
			settingsWindowForm.show_all();
		}

		private void resetButtonAction(){
			imageResult.set_from_file(Application.AssetLocation+"PlaceHolder.png");
			
			sumField1.set_text("");
			sumField2.set_text("");
			
			sumField1.set_sensitive(true);
			sumField2.set_sensitive(true);
			compareButton.set_sensitive(true);
		}

		private bool before_destroy(){
			int posX, posY;
			this.get_position(out posX, out posY);

			mySettings.set_int("pos-x", posX);
			mySettings.set_int("pos-y", posY);
			mySettings.set_int("verification-selected", verificationSUM);
			mySettings.set_boolean("ignore-capitalization", checkButton1.get_active());
			return false;
		}

		private void getMD5Sum(File fileLocation){
			string fileLocationS=fileLocation.get_path();
			
			GLib.Checksum MD5Sum=new GLib.Checksum(GLib.ChecksumType.MD5);
			FileStream fileToRead=FileStream.open(fileLocationS, "rb");
			uint8 fileBuffer[100];
			size_t Size;

			while((Size=fileToRead.read(fileBuffer))>0){
				MD5Sum.update(fileBuffer, Size);
			}
			sumField1.set_text(MD5Sum.get_string());
			sumField1.set_sensitive(false);
		}

		private void getSHA1Sum(File fileLocationSHA1){
			string fileLocationS=fileLocationSHA1.get_path();

			GLib.Checksum SHA1Sum=new GLib.Checksum(GLib.ChecksumType.SHA1);
			FileStream fileToRead=FileStream.open(fileLocationS, "rb");
			uint8 fileBuffer[100];
			size_t Size;

			while((Size=fileToRead.read(fileBuffer))>0){
				SHA1Sum.update(fileBuffer, Size);
			}
			sumField1.set_text(SHA1Sum.get_string());
			sumField1.set_sensitive(false);
		}

		private void getSHA256Sum(File fileLocationSHA256){
			string fileLocationS=fileLocationSHA256.get_path();

			GLib.Checksum SHA256Sum=new GLib.Checksum(GLib.ChecksumType.SHA256);
			FileStream fileToRead=FileStream.open(fileLocationS, "rb");
			uint8 fileBuffer[100];
			size_t Size;

			while((Size=fileToRead.read(fileBuffer))>0){
				SHA256Sum.update(fileBuffer, Size);
			}
			sumField1.set_text(SHA256Sum.get_string());
			sumField1.set_sensitive(false);
		}

		private void aboutButtonAction(){
			AboutWindow aboutForm=new AboutWindow();
			aboutForm.set_transient_for(this);
			aboutForm.show_all();
		}
	}
}
