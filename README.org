* Integrity Check - Vinari Software [GTK version]

[[https://gitlab.com/vinarisoftware/integrity-check-gtk/-/tree/master][MASTER-Branch]]

+ Integrity Check - Vinari Software [GTK version] is a GUI application designed to get the verification sum of a file (MD5, SHA1, SHA256) and compare to the sum of verification provided, and by doing this, it can determine if the file you are working on is corrupted or not.

** Compilation/dependencies and installation guide: 

**** In order to compile this project you will need the following packages
	+ Vala compiler (valac)
	+ GNU Compiler Collection (gcc)
	+ Meson (meson)
	+ Ninja (ninja)

*** Compilation dependencies: 
	+ libglib2.0-0 (libglib2.0-dev)
	+ gtk-3-0 (libgtk-3-dev)
	+ gettext & gettext-base
	+ libgdk-pixbuf2.0-0 (libgdk-pixbuf2.0-dev)

*** Compilation & installation guide:

+ You need to open a terminal in the root directory of the project, tipically is called "integrity-check-gtk-master", once you're there, you'll need to run the following commands:

#+BEGIN_SRC sh
$ meson build --buildtype=release
$ cd build/
$ ninja
$ cd ..
$ sudo ./INSTALL
#+END_SRC

* Contact with the developers

| Contact                | Link                                                            |
|------------------------+-----------------------------------------------------------------|
| Vinari Software (Mail) | vinarisoftware@protonmail.com                                   |
| Vinari OS (Mail)       | vinarios@protonmail.com                                         |
| Vinari Software (Chat) | https://vinarisoftware.wixsite.com/vinari/contacta-con-nosotros |
| Vinari OS (Chat)       | https://vinarisoftware.wixsite.com/vinari-os/contactenos        |


